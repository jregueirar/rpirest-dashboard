from django.conf import settings
from .models import AttachedDevices

def global_settings(request):
    # return any necessary values
    list=[]
    for i in AttachedDevices.objects.all():
        list.append(i.type)

    return {
        'DEVICES_ATTACHED': list,
        'attached_devices_list': AttachedDevices.objects.all(),
        'RPIREST_URL': settings.RPIREST_URL,
        'RPIREST_USER': settings.RPIREST_USER,
        'RPIREST_PASSWD': settings.RPIREST_PASSWD,
    }
