from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rpirest_dashboard.settings import GRAPHITE_PORT, STANDALONE, RPIREST_URL
import logging
from urllib.parse import urlparse

logger = logging.getLogger("dashboard")

# Create your views here.
@login_required(login_url="accounts/login")
def home(request):
    return render(request, "dashboard/home.html")


def context(request, type_device):
    preffix = type_device + "/env_sensor"
    context = {
        'type_device': type_device,
        'resource_temperature': preffix + "/temperature/",
        'resource_humidity': preffix + "/humidity/",
        'resource_pressure': preffix + "/pressure/"
    }
    if STANDALONE:
        url = request.build_absolute_uri()
        rpirest_url = RPIREST_URL.replace(urlparse(RPIREST_URL).hostname, urlparse(url).hostname)
    else:
        rpirest_url = RPIREST_URL

    logger.debug(context)
    context['rpirest_url'] = rpirest_url
    return context

# CONTEXT for graphite querys
# graphite in a local installation
def context_graphite(request, type_device):
    c = context(request, type_device)

    r_temperature = c['resource_temperature'][0:len(c['resource_temperature']) - 1]
    r_humidity = c['resource_humidity'][0:len(c['resource_humidity']) - 1]
    r_pressure= c['resource_pressure'][0:len(c['resource_pressure']) - 1]
    rpi_fqdn = urlparse(request.build_absolute_uri('/')).hostname
    scheme = urlparse(request.build_absolute_uri('/')).scheme
    c['graphite_server'] = scheme + "://" + rpi_fqdn + ":" + GRAPHITE_PORT + "/"
    c['graphite_target_temperature'] = rpi_fqdn + "." + r_temperature.replace('/', '.')
    c['graphite_target_humidity'] = rpi_fqdn + "." + r_humidity.replace('/', '.')
    c['graphite_target_pressure'] = rpi_fqdn + "." + r_pressure.replace('/', '.')

    logger.debug(c)
    return c

@login_required(login_url="accounts/login")
def rpi(request, type_device):
    return render(request, 'dashboard/rpi.html', context(request, type_device))


@login_required(login_url="accounts/login")
def sensehat(request, type_device):
    return render(request, 'dashboard/env_sensor.html', context_graphite(request, type_device))


@login_required(login_url="accounts/login")
def sensehat_led_matrix(request, type_device):
    return render(request, 'dashboard/sensehat_led_matrix.html', context(request, type_device))


@login_required(login_url="accounts/login")
def dht(request, type_device):
    return render(request, 'dashboard/env_sensor.html', context(request, type_device))
