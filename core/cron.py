# Si hay algun dispotivo tipo sensehat => monitorizamos temperatura, humedad y presión
# Si hay algún dispotivo tipo am2302, dht11 o dht22 => monitorizamos temperatura y humedad
# Futuro: Si hay rpi => monitorizamos cpu, uso memoria .
from unipath import Path
import requests
import yaml
import kronos
import graphitesend
from dashboard.models import AttachedDevices
from rpirest_dashboard.settings import RPIREST_URL, RPIREST_PASSWD, RPIREST_USER
import logging
from urllib.parse import urlparse

logger = logging.getLogger("cron_tasks")

BASE_DIR = Path(__file__).parent
ymlconfig = BASE_DIR.child("collector_config.yml")
with open(ymlconfig, 'r') as ymlfile:
    cfg = yaml.load(ymlfile)


def get_metrics_from_device(type_device):
    metrics = {}

    api_url = RPIREST_URL + type_device + "/env_sensor/temperature/"
    logger.debug("api_url: " + api_url)
    rTemp = requests.get(api_url, auth=(RPIREST_USER, RPIREST_PASSWD))
    data = rTemp.json()
    logger.debug('Result: ' + str(data['result']))
    metrics['temperature'] = data['result']

    api_url = RPIREST_URL + type_device + "/env_sensor/humidity/"
    logger.debug("api_url: " + api_url)
    rTemp = requests.get(api_url, auth=(RPIREST_USER, RPIREST_PASSWD))
    data = rTemp.json()
    logger.debug('Result: ' + str(data['result']))
    metrics['humidity'] = data['result']

    if type_device == 'sensehat':
        api_url = RPIREST_URL + type_device + "/env_sensor/pressure/"
        logger.debug("api_url: " + api_url)
        temp = requests.get(api_url, auth=(RPIREST_USER, RPIREST_PASSWD))
        data = temp.json()
        logger.debug('Result: ' + str(data['result']))
        metrics['pressure'] = data['result']

    return metrics


@kronos.register('* * * * *')
def send2graphite():
    # The separator in the metrics of graphite is '.'
    hostname = urlparse(RPIREST_URL).hostname.replace('.', '_')
    for type_device in ('am2302', 'dht11', 'dht22', 'sensehat'):
        if AttachedDevices.objects.filter(type=type_device):
            for server in cfg['graphite']['servers']:
                prefix = hostname + '.' + type_device + '.env_sensor'
                g = graphitesend.init(graphite_server=server,  prefix=prefix, system_name='')
                msg = g.send_dict(get_metrics_from_device(type_device))
                logger.debug(msg)
